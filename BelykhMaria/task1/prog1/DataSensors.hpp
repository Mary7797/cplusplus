#ifndef _SENSOR_HPP_
#define _SENSOR_HPP_


#include<map>
#include<string>
#include<ctime>
#include<cstdlib>

using namespace std;

class DataSensors {
public:
	~DataSensors();
	DataSensors(int =0);

	string getModel() const;
	string getCoord() const;
	string getDate() const;
	double getData() const;

private:
	string model;
	string coord;
	string date;
	int type;
};
#endif