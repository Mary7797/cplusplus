#include<iostream>
#include<string>
#include <fstream>
#include"DataSensors.hpp"

int main()
{
	cout << "Enter a number of sensors: ";
	int countSensors;
	cin >> countSensors;

	ofstream fout("Sensor.csv", ios::app);
	fout << "Model; Coordinates; Date; Data\n" << endl;

	for (int i = 0; i < countSensors; ++i)
	{
		cout << "Enter type of sensor (1 - Temp, 2 - Wet): ";
		int type;
		cin >> type;

		DataSensors sensor(type);

		fout << sensor.getModel() << ";" << sensor.getCoord() << ";" << sensor.getDate() << ";";
		for (int j = 0; j < rand() % 10; ++j)
			fout << sensor.getData() << ";";
		fout << endl;

		sensor.~DataSensors();
	}
	fout.close();
	return 0;
}