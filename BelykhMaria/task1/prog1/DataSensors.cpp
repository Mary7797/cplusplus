#include"DataSensors.hpp"

DataSensors::~DataSensors()
{
	model = coord = date = "";
	type = 0;
}

DataSensors::DataSensors(int t)
{
	srand(time(nullptr));
	type = t;
}

string DataSensors::getModel() const
{
	string model;
	char letter[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char num[] = "0123456789";
	int lenLetter = sizeof(letter);
	int lenNum = sizeof(num);

	for (int i = 0; i < rand() % 4; ++i)
		model += letter[rand() % lenLetter];
	model += '-';
	for (int j = 0; j < rand() % 5; ++j)
		model += num[rand() % lenNum];

	return model;
}

string DataSensors::getCoord() const
{
	double x = static_cast<double>(rand() % 300001) / 10000;
	double y = static_cast<double>(rand() % 600001) / 10000;

	string coord = to_string(x) + ", " + to_string(y);

	return coord;
}

string DataSensors::getDate() const
{
	time_t currTime = time(0);
	tm *ltm = localtime(&currTime);
	ltm->tm_mday = rand() % 3650 * -1;
	time_t next = mktime(ltm);
	ltm = localtime(&next);
	char buf[50];

	strftime(buf, sizeof(buf), "%Y-%m-%d", ltm);

	return buf;
}

double DataSensors::getData() const
{
	if (type == 1)
		return static_cast<double>(rand() % 81 - 40) / 100;
	else return static_cast<double>(rand() % 111 - 55) / 1000;
}

